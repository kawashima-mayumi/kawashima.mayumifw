package jp.co.FW.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import jp.co.FW.entity.UserEntity;

@Component
public interface UserMapper {

	void register(UserEntity user);

	UserEntity getLoginUser(@Param("login_id") String login_id, @Param("password") String password);
}
