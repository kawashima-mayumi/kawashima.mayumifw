package jp.co.FW.form;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class SignupForm {
	private Integer id;
	@NotEmpty
	@Size(min=6, max=20)
	@Pattern(regexp = "^[0-9a-zA-Z]+$")
	private String login_id;
	@NotEmpty
	@Size(max = 10)
	private String name;
	@NotEmpty
	@Size(min = 6, max = 60)
	private String password;
	@NotEmpty
	private String re_password;
	@NotEmpty
	private int branch_id;
	@NotEmpty
	private int deployment_id;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public int getDeployment_id() {
		return deployment_id;
	}
	public void setDeployment_id(int deployment_id) {
		this.deployment_id = deployment_id;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRe_password() {
		return re_password;
	}
	public void setRe_password(String re_password) {
		this.re_password = re_password;
	}

}
