package jp.co.FW.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.FW.dto.BranchDto;
import jp.co.FW.dto.DeploymentDto;
import jp.co.FW.dto.UserDto;
import jp.co.FW.dto.factroty.BranchDtoFactory;
import jp.co.FW.dto.factroty.DeploymentDtoFactory;
import jp.co.FW.entity.factory.UserEntityFactory;
import jp.co.FW.mapper.BranchMapper;
import jp.co.FW.mapper.DeploymentMapper;
import jp.co.FW.mapper.UserMapper;

;

@Service
public class SignupService {

	 @Autowired
	    private UserMapper userMapper;
	 @Autowired
	    private UserEntityFactory userEntityFactory;
	 @Autowired
	 	private BranchMapper branchMapper;
	 @Autowired
	 	private BranchDtoFactory branchDtoFactory;
	 @Autowired
	 	private DeploymentMapper deploymentMapper;
	 @Autowired
	 	private DeploymentDtoFactory deploymentDtoFactory;

	 public void registUser(UserDto userDto) {
	        userMapper.register(userEntityFactory.encPasswordUser(userDto));
	 }

	 public List<BranchDto> getBranchList(){
		 return branchDtoFactory.create(branchMapper.getAll());
	 }

	 public List<DeploymentDto> getDeploymentList(){
		 return deploymentDtoFactory.create(deploymentMapper.getAll());
	 }
}
