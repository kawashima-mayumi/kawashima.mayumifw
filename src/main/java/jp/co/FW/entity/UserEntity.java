package jp.co.FW.entity;

import java.util.Date;

public class UserEntity {
private Integer id;
private String name;
private String login_id;
private String password;
private Integer branch_id;
private Integer deployment_id;
private Integer is_stopped;
private Date create_date;
private Date update_date;

	public UserEntity(Integer id, String name, String login_id, String password, Integer branch_id, Integer deployment_id,
			Integer is_stopped, Date create_date, Date update_date) {

		this.id = id;
		this.name = name;
		this.login_id = login_id;
		this.password = password;
		this.branch_id = branch_id;
		this.deployment_id = deployment_id;
		this.is_stopped = is_stopped;
		this.create_date = create_date;
		this.update_date = update_date;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getBranch_id() {
		return branch_id;
	}

	public void setBranch_id(Integer branch_id) {
		this.branch_id = branch_id;
	}

	public Integer getDeployment_id() {
		return deployment_id;
	}

	public void setDeployment_id(Integer deployment_id) {
		this.deployment_id = deployment_id;
	}

	public Integer getIs_stopped() {
		return is_stopped;
	}

	public void setIs_stopped(Integer is_stopped) {
		this.is_stopped = is_stopped;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
