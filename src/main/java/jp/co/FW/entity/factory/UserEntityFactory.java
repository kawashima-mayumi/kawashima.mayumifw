package jp.co.FW.entity.factory;

import org.springframework.stereotype.Component;

import jp.co.FW.dto.UserDto;
import jp.co.FW.entity.UserEntity;
import jp.co.FW.utils.CipherUtil;

@Component
public class UserEntityFactory {

	public UserEntity encPasswordUser(UserDto userDto) {

		return new UserEntity(
				null,
				userDto.getName(),
				userDto.getLogin_id(),
				CipherUtil.encrypt(userDto.getPassword()),
				userDto.getBranch_id(),
				userDto.getDeployment_id(),
				0,
				null,
				null
				);
	}

}
