package jp.co.FW.dto.factroty;

import org.springframework.stereotype.Component;

import jp.co.FW.dto.UserDto;
import jp.co.FW.entity.UserEntity;
import jp.co.FW.form.SignupForm;

@Component
public class UserDtoFactory {

	public UserDto create(SignupForm signupForm) {

		return new UserDto(
				null,
				signupForm.getName(),
				signupForm.getLogin_id(),
				signupForm.getPassword(),
				signupForm.getBranch_id(),
				signupForm.getDeployment_id()
				);
	}

	public UserDto create(UserEntity user) {
		if(user == null) {
			return null;
		}
		return new UserDto(
				user.getId(),
				user.getName(),
				user.getLogin_id(),
				user.getPassword(),
				user.getBranch_id(),
				user.getDeployment_id()
				);
	}


}
